# TypeFlash

This is an application implemented using python and libraries time and streamlit.

This calculates your typing speed 🚀 in words-per-minute (WPM) speed!

I deployed this on streamlit.

https://whirlx.streamlit.app/
